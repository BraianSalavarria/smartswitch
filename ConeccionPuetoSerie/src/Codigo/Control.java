package Codigo;

import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import java.util.ArrayList;
import javax.swing.JComboBox;
import jssc.SerialPortException;


public class Control {

private String puertoSeleccionado; 
private ArrayList<String> puertos;
private PanamaHitek_Arduino arduino;

public static int estadoBtnLuces=0;
public static int estadoBtnLuz1=0;
public static int estadoBtnLuz2=0;

public Control(){
    
    puertos = new ArrayList<String>();
    arduino = new PanamaHitek_Arduino();
    
   
}

public void obtenerPuertos(JComboBox combo){
    
    puertos = (ArrayList<String>) arduino.getSerialPorts();
    
    for (int i =0;i< puertos.size();i++){
        
        combo.addItem((String) puertos.get(i));
    }
    
    
}

public  void conectar(String puerto) throws ArduinoException{

arduino.arduinoTX(puerto, 9600);
    
}

public  void apagarTodo() throws ArduinoException, SerialPortException{
    
    arduino.sendData("5");
}
public  void encenderTodo() throws ArduinoException, SerialPortException{
    
    arduino.sendData("4");
}


public  void encenderLuz1() throws ArduinoException, SerialPortException{
    
    arduino.sendData("1");
}
public  void apagarLuz1() throws ArduinoException, SerialPortException{
    
    arduino.sendData("0");

}


public  void encenderLuz2() throws ArduinoException, SerialPortException{
    
    arduino.sendData("2");
}
public  void apagarLuz2 () throws ArduinoException, SerialPortException{
    arduino.sendData("3");
}


public  void botonLuces() throws ArduinoException, SerialPortException{
    
    if(estadoBtnLuces ==0 ) {
        
        encenderTodo();
        estadoBtnLuces=1;
        estadoBtnLuz1=1;
        estadoBtnLuz2=1;
    
    }else{
    
     if(estadoBtnLuces==1) {
         
         apagarTodo();
         estadoBtnLuces =0;
         estadoBtnLuz1=0;
        estadoBtnLuz2=0;
        }     
    }  
}


public  void botonLuz1() throws ArduinoException, SerialPortException{
 
    if(estadoBtnLuz1 == 0){
         encenderLuz1();
         estadoBtnLuz1=1;
    
    }else{
     
        apagarLuz1();
        estadoBtnLuz1=0;
    }
}

public  void botonLuz2() throws ArduinoException, SerialPortException{
   
   if(estadoBtnLuz2==0){
        encenderLuz2();
        estadoBtnLuz2=1;
   
   }else{
       
    if(estadoBtnLuz2==1){
        apagarLuz2();
        estadoBtnLuz2=0;
       }
   }
}






}
